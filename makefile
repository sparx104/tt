
#
# Public domain
#

CC=gcc
CFLAGS=-g -Wall -Wextra 
LFLAGS=$(CFLAGS)

all: tt
	@printf "\033[7m%s\033[0m\n" "$@"

clean:
	@printf "\033[7m%s\033[0m\n" "$@"
	rm -f tt.o tt

install: install-bin install-man
	@printf "\033[7m%s\033[0m\n" "$@"

install-bin:
	@printf "\033[7m%s\033[0m\n" "$@"
	cp tt /usr/local/bin/

install-man:
	@printf "\033[7m%s\033[0m\n" "$@"
	cp tt.1.gz /usr/local/man/man1/

# you can test with this (or man ./tt.1)
# groff -man -Tascii ./tt.1
#
man:
	@printf "\033[7m%s\033[0m\n" "$@"
	rm -f tt.1.gz
	gzip -c tt.1 >| tt.1.gz

tt: tt.o mxl.o
	@printf "\033[7m%s\033[0m\n" "$@"
	$(CC) $(LFLAGS) -o $@  $^

tt.o: src/tt.c
	@printf "\033[7m%s\033[0m\n" "$@"
	$(CC) $(CFLAGS)  -c $<

mxl.o: src/mxl.c src/mxl.h
	@printf "\033[7m%s\033[0m\n" "$@"
	$(CC) $(CFLAGS)  -c $<
