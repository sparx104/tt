# tt

Tagged type processor

# What is this?

This is a simple tool to providing a means to "tag" variables with a type and 
use that type in function calls.

It's mainly to save typing.

It is also not very clever.

# Background

In c it's common to create a "module" by prefixing all the functons with the
module name and make the first variable in each call point to a struct 
containing the module's information, eg. string_crop( self, from, length ).

This allows you to tag a variable with a "type" and have that type expand to
the original function call.  Eg.

    char *x = "Hello", *y;
    y = string_crop( x, 0, 10 );
    string_free( y );

Can be replaced with

    char *x = "Hello", *y;
    !string x, y
    y = x.crop( 0, 10 );
    y.free();

To save a bit of typing and make the code look better.

# How it works

Each line of the input is read, any starting ! are parsed for the definitions.

Each word ending `(` (the `(` has to be attached to the call) is checked to see 
if there is a "." in it, if so the type is looked up and replaced in this 
format:

    "<variable>.<call>(" 
    
becomes 

    "<variable_type>_<call>( [<parents>, ] <variable>[,]"

# Manual

See manual.txt for information on usage
    
    
