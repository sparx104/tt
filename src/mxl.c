//
//  mxl
//  Copyright (c)2018 by Chris Dickens
//
//  Cut down version of libmacro.  Performs the basic macro expansion
//  without inline definition, commands or conditionals.  Also doesn't
//  trim whitespace outside of [[ ]] to ensure line number is not changed.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "strutils.h"
#include "mxl.h"

// The block size for appending to the output
#define A_SIZE 128


typedef struct pair pair_t;

struct pair {
        struct pair *next;
        char key[64];
        char *value;
    };

struct mxl {
        struct pair *dict;
        char error[256];
        char *arg_idx_buf;
    };

static char * find_close( const char * );
static const char * datetime( char );
static char * get_arg_idx( const char *, int );
static const char * mxl_convert_special( mxl_t *, const char * );

static int mxl_expand_step(
        mxl_t *, const char *, struct strbuild *
    );

// !!! .find_close.private
// Find the first unmatched ]] - this is the closing one as the opening [[
// must not be included.
//
// Nesting is supported so you can't have extra [[ or ]].  Use [[:open:]] or
// [[:close:]] to insert them later.
//
// RETURNS: pointer to the closing ]]
//
static char *find_close(
    const char *str
) {
    int depth;
    //
    depth = 1;
    while( *str ) {
        if( (*str == '[') && (*(str + 1) == '[') ) {
            // increase nesting and skip the [[
            depth += 1;
            str += 2;
            //
        } else if( (*str == ']') && (*(str + 1) == ']') ) {
            // "]]"
            depth -= 1;
            if( depth == 0 ) {
                // we're done
                // const-cast - same as strchr, not nice.
                return (char *)str;
            } else {
                // close an inner scope, skip the ]] and continue
                str += 2;
            }
            //
        } else {
            // something else
            str += 1;
        }
    }
    //
    // no ]] found
    return NULL;
}

// !!! .datetime.private
// Get the time/date or part depending on TYP:
//
//  D       the date as yyyymmdd
//  T       the time as hhiiss
//  h, i, s the hour, minute or second as 2 digits
//  d, m    the day or month as 2 digits
//  y       the year as 4 digits
//
// NOTE:    the returned value is stored in a internal buffer, it will be
//          overwritten on each call to this - you'll need to copy it if you
//          want it to hang around.
//
// RETURNS: requested value or NULL if TYP was invalid
//
static const char * datetime(
    char typ
) {
static char buf[9];
    //
    time_t epochtime;
    struct tm *now;
    //
    time( &epochtime );     // load now into "epochtime"
    now = localtime( &epochtime );
    switch( typ ) {
        case 'D':
            snprintf( 
                    buf, 9, "%04d%02d%02d",
                    (now->tm_year + 1900), (now->tm_mon + 1), now->tm_mday
                );
            break;
            //
        case 'T':
            snprintf(
                    buf, 9, "%02d%02d%02d",
                    now->tm_hour, now->tm_min, now->tm_sec
                );
            break;
            //
        case 'y':
            snprintf( buf, 9, "%04d", (now->tm_year + 1900) );
            break;
            //
        case 'm':
            snprintf( buf, 9, "%02d", (now->tm_mon + 1) );
            break;
            //
        case 'd':
            snprintf( buf, 9, "%02d", now->tm_mday );
            break;
            //
        case 'h':
            snprintf( buf, 9, "%02d", now->tm_hour );
            break;
            //
        case 'i':
            snprintf( buf, 9, "%02d", now->tm_min );
            break;
            //
        case 's':
            snprintf( buf, 9, "%02d", now->tm_sec );
            break;
            //
        default:
            return NULL;
    }
    return buf;
}

// !!! .insert_arg.private
// Insert the arguments from ARG into STR and return the heap allocated
// result.
//
// % strings are:
//  %%      - "%"
//  %@/%*   - ARG
//  %n      - the n'th part of ARG separated with |.  If not found "" is used
//
// RETURNS: heap-allocated result
//
static char * insert_arg(
    const char *str,
    const char *arg
) {
#define APPEND( _v_, _l_ )  \
    if( !strbuild_nappend( &sb, (_v_), (_l_) ) ) {  \
        return NULL;  \
    }
    //
    const char *br;
    struct strbuild sb;
    char *arg_part;
    size_t arg_len;
    //
    arg_len = strlen( arg );
    //
    sb = (struct strbuild) {
            .value = NULL,
            .size = 0,
            .length = 0,
            .grow = A_SIZE
        };
    //
    // run through the string building the output
    while( (br = strchr( str, '%' )) ) {
        APPEND( str, (br - str) );
        if( br[1] == '%' ) {
            APPEND( "%", 1 );
            //
        } else if( (br[1] == '@') || (br[1] == '*') ) {
            APPEND( arg, arg_len );
            //
        } else if( (br[1] >= '0') && (br[1] <= '9') ) {
            arg_part = get_arg_idx( arg, (br[1] - '0') );
            APPEND( arg_part, strlen( arg_part ) );
            free( arg_part );
            //
        } else {
// TODO:            SET_ERROR( "Invalid % sequence" );
            free( sb.value );
            return 0;
        }
        //
        str = (br + 2);
    }
    APPEND( str, strlen( str ) );
    return sb.value;
    //
#undef APPEND
}

// !!! .mxl_convert_special.private
// Convert some special names into strings.  The result of these WILL NOT
// be expanded.
//
// RETURNS: the string to insert or NULL if the name was not found
//
static const char * mxl_convert_special(
    mxl_t *self,
    const char *name
) {
#define IS( _s_, _l_ ) ( \
        (strncmp( name, (_s_), (_l_) ) == 0) && \
        ((name[_l_] == '\0') || (name[_l_] == ':')) \
    )
    //
    if( name[1] == '\0' ) {
        // single char name
        switch( name[0] ) {
            case '(':       return "[[";
            case ')':       return "]]";
            case '%':       return "%";
        }
        return NULL;
    }
    //
    if( *name == ':' ) {
        name += 1;
    }
    if( IS( "open", 4 ) ) {
        return "[[";
    } else if( IS( "close", 5 ) ) {
        return "]]";
    } else if( IS( "newline", 7 ) || IS( "nl", 2 ) ) {
        return "\n";
    } else if( IS( "tab", 3 ) ) {
        return "\t";
    } else if( IS( "space", 5 ) || IS( "spc", 3 ) ) {
        return " ";
        //
    } else if( IS( "time", 4 ) || IS( "date", 4 ) ) {
        return datetime( (*name - 32) );
    } else if(
            IS( "hour", 4 ) || IS( "second", 6 ) || IS( "sec", 3 ) ||
            IS( "day", 3 ) || IS( "month", 5 ) || IS( "year", 4 )
        ) {
        return datetime( *name );
    } else if( IS( "minute", 6 ) || IS( "min", 3 ) ) {
        return datetime( 'i' );
    } else if( *name == '$' ) {
        // try the environment
        return getenv( (name + 1) );
        //
    } else if( IS( "line", 4 ) ) {
        // TT: ":line[:]" is __line__
        return mxl_get_define( self, "__line__" );
    } else if( IS( "file", 4 ) ) {
        // TT: ":file[:]" is __file__
        return mxl_get_define( self, "__file__" );
    } else if( IS( "output", 6 ) ) {
        // TT: ":output[:]" is __output__
        return mxl_get_define( self, "__output__" );
        //
    } else {
        return NULL;
    }
//
#undef IS
}

// !!! .get_arg_idx.private
// Get the IDX'th argument from STR.  Arguments are separated with "|".
//
// If the argument doesn't exist "" is returned
//
// RETURNS: heap allocated string or NULL on error
//
static char * get_arg_idx(
    const char *str,
    int idx
) {
    const char *end;
    char *out;
    size_t len;
    //
    // Find the IDX'th marker trimming the initial WS
    while( idx > 0 ) {
        str = strchr( str, '|' );
        if( !str ) {
            len = 0;
            goto save;
        }
        str += 1;
        strmovenws( str );
        //
        idx -= 1;
    }
    //
    // Find the ending marker, copy and trim
    end = strchr( str, '|' );
    if( end ) {
        len = (end - str);
    } else {
        len = strlen( str );
    }
    //
save:
    out = malloc( ((len + 1) * sizeof( char )) );
    if( !out ) {
        return NULL;
    }
    //
    strnset( out, (len + 1), str, len );
    strtrim( out );
    return out;
}

// !!! .mxl_new
// Create a new macro object
mxl_t * mxl_new(
    void
) {
    mxl_t *m;
    m = calloc( 1, sizeof( mxl_t ) );
    if( !m ) {
        return NULL;
    }
    //
    return m;
}

// !!! .mxl_free
// free() a macro object
//
void mxl_free(
    mxl_t *self
) {
    if( !self ) {
        return;
    }
    //
    free( self->arg_idx_buf );
    //
    pair_t *p, *pn;
    p = self->dict;
    while( p ) {
        pn = p->next;
        free( p->value );
        free( p );
        //
        p = pn;
    }
    //
    free( self );
}

// !!! .mxl.define
// Set a definition in this macro.  This will replace any definition with
// the same KEY.
//
// RETURNS: the added pair or NULL on error
//
int mxl_define(
    mxl_t *self,
    const char *key,
    const char *value
) {
    pair_t *p;
    char *nv, *key_buf;
    size_t len, orig_len;
    int is_add;
    //
    is_add = 0;
    key_buf = NULL;
    len = strlen( key );
    if( (len > 0) && (key[len - 1] == '+') ) {
        is_add = 1;
        key_buf = strndup( key, (len - 1) );
        key = key_buf;
    }
    //
    len = strlen( value );
    //
    p = self->dict;
    while( p ) {
        if( strcmp( p->key, key ) == 0 ) {
            // Replace existing define
            if( is_add ) {
                orig_len = strlen( p->value );
            } else {
                orig_len = 0;
            }
            nv = realloc( p->value, ((orig_len + len + 1) * sizeof( char )) );
            if( !nv ) {
                free( key_buf );
                return 0;
            }
            p->value = nv;
            //
            if( is_add ) {
                nv += orig_len;
            }
            memcpy( nv, value, len );
            nv[len] = '\0';
            free( key_buf );
            return 1;
        }
        p = p->next;
    }
    //
    // Add new
    p = malloc( sizeof( pair_t ) );
    if( !p ) {
        free( key_buf );
        return 0;
    }
    //
    strncpy( p->key, key, 64 );
    p->key[63] = '\0';
    free( key_buf );
    //
    p->value = malloc( ((len + 1) * sizeof( char )) );
    if( !(p->value) ) {
        free( p );
        return 0;
    }
    memcpy( p->value, value, len );
    p->value[len] = '\0';
    //
    p->next = self->dict;
    self->dict = p;
    return 1;
}

// !!! .mxl.define_l
// Set a definition in this macro.  This will replace any definition with
// the same KEY
//
// RETURNS: the added pair or NULL on error
//
int mxl_define_l(
    mxl_t *self,
    const char *key,
    long value
) {
static char buf[32];
    snprintf( buf, 32, "%ld", value );
    return mxl_define( self, key, buf );
}

// !!! .mxl.define_str
// Set a definition in this macro.  This will replace any definition with
// the same KEY
//
// RETURNS: the added pair or NULL on error
//
int mxl_define_str(
    mxl_t *self,
    const char *str,
    size_t str_len
) {
    char *buf, *key, *value;
    int ok;
    //
    buf = strndup( str, str_len );
    strtrim( buf );
    value = strchr( buf, '=' );
    if( !value ) {
        free( buf );
        return 0;
    }
    //
    key = buf;
    strmovenws( key );
    *(value++) = '\0';
    //
    ok = mxl_define( self, key, value );
    free( buf );
    //
    return ok;
}

// !!! .mxl_get_define
// Get the value of the given define
//
// RETURNS: the value or NULL if not found
//
const char * mxl_get_define(
    const mxl_t *self,
    const char *key
) {
    pair_t *p;
    p = self->dict;
    while( p ) {
        if( strcmp( p->key, key ) == 0 ) {
            return p->value;
        }
        //
        p = p->next;
    }
    //
    return NULL;
}
    
// !!! .mxl_get_last_error
// Get the last error that occured when <mxl_expand()> failed.  This
// is reset to "" each time <mxl_expand()> succeeds.
//
// NOTE:    The error is stored in the (mxl_t *) SELF, once you've free'ed
//          it the error string will be invalid.
//
// RETURNS: the last error from <mxl_expand()> or "" if none occured.
//
const char * mxl_get_last_error(
    const mxl_t *self
) {
    return self->error;
}

// !!! .mxl_expand
// Expand the macros in the given string recusively
//
// ARG will be used for [[:arg:]]
//
// RETURNS: heap-allocated result
//
char * mxl_expand(
    mxl_t *self,
    const char *str
) {
    struct strbuild sb;
    //
    // Set up the output
    sb = (struct strbuild) {
            .value = NULL,
            .length = 0,
            .size = 0,
            .grow = A_SIZE
        };
    //
    // Clear the last error
    self->error[0] = '\0';
    //
    // Start expanding
    if( !(mxl_expand_step( self, str, &sb )) ) {
        // error expanding somewhere.  The error has been set but we
        // need to clean up the output
        free( sb.value );
        sb.value = NULL;
    }
    //
    return sb.value;
}

// !!! .mxl_expand_step.private
// Expand the macros in the given string recusively
//
// NOTE:    @, # and ~ will trim all whitespace after themselves
//          automatically - this allows them to appear on lines without
//          breaking flow.  Normal expansions, commands and ? do not
//          trim after they've been inserted - you can use [[~]].
//
// RETURNS: heap-allocated result
//
static int mxl_expand_step(
    mxl_t *self,
    const char *str,
    struct strbuild *sb
) {
#define APPEND( _v_, _l_ )  \
    if( !strbuild_nappend( sb, (_v_), (_l_) ) ) { \
        strset( self->error, 256, "Unable to append value" );  \
        return 0;  \
    }
    //
    const char *start, *end;
    const char *value, *spec;
    char *repl_value;
    char *name, *arg;
    size_t name_len, name_sz;
    int ok;
    //
    name = NULL;
    name_sz = 0;
    while( (start = strstr( str, "[[" )) ) {
        APPEND( str, (start - str) );
        //
        // move str to after [[ then left trim
        str = (start + 2);
        strmovenws( str );
        end = find_close( str );
        if( !end ) {
            // no ending ]]/@]]
            strset( self->error, 256, "Unclosed macro" );
            return 0;
        }
        //
        // A comment?
        if( *str == '#' ) {
            // comment 
            str = (end + 2);
            continue;
        }
        //
        if( *str == '@' ) {
            str += 1;
            strmovenws( str );
            if( !mxl_define_str( self, str, (end - str) ) ) {
                snprintf(
                        self->error, 256,
                        "Failed to define '%.*s'", (int)(end - str), str
                    );
                return 0;
            }
            str = (end + 2);
            continue;
        }
        //
        name_len = (end - str);
        if( !strnbuf( &name, &name_sz, str, name_len ) ) {
            strset( self->error, 256, "Memory error" );
            return 0;
        }
        //
        // move str to point to after the ]], we don't use it again until
        // we search for the next [[.
        str = (end + 2);
        //
        strtrim( name );
        //
        // Empty ("[[]]" is ignored)
        if( !(name[0]) ) {
            continue;
        }
        //
        // ":<name>[:]" or ()% are "special" macros.. We process them
        // out of sequence so they don't get expanded further and break
        // things.  If SPEC ends up NULL then this wasn't a special macro, if
        // NAME did start : then we'll error as undefined
        spec = mxl_convert_special( self, name );
        if( spec ) {
            APPEND( spec, strlen( spec ) );
            continue;
        }
        //
        // Determine if there's an arg and split if so
        arg = strchr( name, ' ' );
        if( arg ) {
            *(arg++) = '\0';
        } else {
            arg = NULL;
        }
        //
        // Normal macro        
        value = mxl_get_define( self, name );
        if( !value ) {
            // undefined
            snprintf( self->error, 256, "'%s' is undefined", name );
            //
            free( name );
            return 0;
        }
        //
        // Expand the argument with :arg: being the arg THIS CALL was given,
        // not the argument specified in the define (eg. what we're expanding)
        // Then expand the define's value using this expanded argument.
//        ex_arg = mxl_expand( self, (arg ? arg : ""), str_arg );
//printf( "%s: '%s'=>'%s'\n", name, arg, str_arg );
//        ok = mxl_expand_step( self, value, ex_arg, out, out_size, out_len );
        if( !arg ) {
            arg = "";
        }
        repl_value = insert_arg( value, arg );
        if( !repl_value ) {
            free( name );
            //
            snprintf( self->error, 256, "Failed to expand '%s'", value );
            return 0;
        }
        ok = mxl_expand_step( self, repl_value, sb );
        free( repl_value );
//        free( ex_arg );
//        free( name );
        if( !ok ) {
            // Error has already been set
            return 0;
        }
    }
    //
    APPEND( str, strlen( str ) );
    free( name );
    //
    return 1;
    //
#undef APPEND
}

#ifdef MXL_EXE

// !!! .main
int main(
    int argc,
    char **argv
) {
    (void) argc;
    (void) argv;
    //
    mxl_t *m;
    m = mxl_new();
    if( !m ) {
        fprintf( stderr, "Out of memory\n" );
        return 1;
    }
    //
    char *out;
    mxl_define( m, "sayhi", "Hello, %%" );
    mxl_define( m, "name", "chr" );
    mxl_define( m, "name+", "is" );
//    mxl_define( m, "name", "[[:time:]]" );
    out = mxl_expand( m, "[[sayhi [[name]]]]!\n[[:date:]]" );
    printf( "%s\n", (out ? out : mxl_get_last_error( m )) );
    free( out );
    //
    mxl_free( m );
    return 0;
}

#endif