#ifndef __STRUTILS_H__
#define __STRUTILS_H__

#include <string.h>

#define MAYBE_UNUSED __attribute__((unused))

// !!! $path c.cycore

//
// Provides various string utilities and a basic stringbuilder/buffer in a
// header file so the functions are local to their callers.
//
// (c)2018 by Chris Dickens
//

// !!! .memclr
// Write Z 0's to S.  This is simply an alias to memset().
//
#define memclr( _s_, _z_ ) memset( (_s_), '\0', (_z_) )
//+++

// !!! .strcws
// Determine if the char C is whitespace (eg. C is not 1-32)
//
// RETURNS: true if C is whitespace, false if it is.
//
#define strcws( _c_ ) (((_c_) > 0) && ((_c_) <= 32))
//+++

// !!! .strmovenws
// Skip any whitespace (1-32) at the start of the string.
//
// NOTE:    this modifies the pointer given - you won't be able to
//          free() the result.
//
#define strmovenws( _s_ ) while( strcws( *(_s_) ) ) { (_s_) += 1; }
//+++

// !!! .strclr
// Write Z \0's to S.  This is simply an alias to memset().
//
#define strclr( _s_, _z_ ) memset( (_s_), '\0', (_z_) )
//+++


// !!! .strnapins
// Write the first AP_LEN bytes of AP into *STR.  If AT_END is 0 AP is
// prepended, if 1 it will be appended.
//
// If REALLOC_STEP_SZ is 0 then *STR will not be resized to fit - *STR_SZ is
// taken as a fixed size.
//
// If REALLOC_STEP_SZ is > 1 then *STR_SZ will have GROW_LEN added to it and
// be rounded to a multiple of REALLOC_SZ and *STR will be realloc()'ed to
// that new size.  If *STR is NULL it will be allocated.
//
// If STR_SZ is NULL then *STR will be realloc()'ed on each call (if
// REALLOC_STEP_SIZE is > 0, if 0 this will fail immediately as it makes no
// sense to try to write to a buffer of 0 length)
//
// If STR_LEN is NULL then 0 is used and AT_END is ignored - the value will
// replace whatever's in *STR.
//
// NOTE:    this function does all the append/prepend work - all the other
//          variations are macros that call this in various ways.
//
// RETURNS: true if the length was able to be accomodated (may realloc()
//          *STR) and the content inserted, false if too big or realloc()
//          failed.
//
static inline int strnapins(
    char **str,
    size_t *str_sz,
    size_t *str_len,
    size_t realloc_step_sz,
    const char *ap,
    size_t ap_len,
    int at_end
) {
    char *new_data;
    size_t new_sz, tmp_sz, tmp_len;
    //
    if( !str_sz ) {
        if( !realloc_step_sz ) {
            // No size and no resizing makes no sense
            return 0;
        }
        //
        // No size - always realloc.
        tmp_sz = 0;
        str_sz = &tmp_sz;
    }
    if( !str_len ) {
        // The contents of *STR will be replaced
        tmp_len = 0;
        str_len = &tmp_len;
    }
    //
    if( realloc_step_sz ) {
        // round up to block step size
        new_sz = (*str_len + ap_len + 1);
        new_sz = (new_sz + (realloc_step_sz - (new_sz % realloc_step_sz)));
        if( new_sz > *str_sz ) {
            // realloc & update *STR and *STR_SZ
            new_data = realloc( *str, (new_sz * sizeof( char )) );
            if( !new_data ) {
                return 0;
            }
            *str = new_data;
            *str_sz = new_sz;
        }
    } else {
        // no realloc - we need to fit within SIZE
        new_sz = (*str_len + ap_len + 1);        // +1 for \0
        if( new_sz > *str_sz ) {
            // won't fit
            return 0;
        }
    }
    //
    if( ap && (ap_len > 0) ) {
        if( at_end || (*str_len == 0) ) {
            memcpy( (*str + *str_len), ap, ap_len );
        } else {
            memmove( (*str + ap_len), *str, *str_len );
            memcpy( *str, ap, ap_len );
        }
        *str_len += ap_len;
    }
    //
    // Always ensure the string is terminated, either after the appended
    // string or at the current length if nothing was appended.
    (*str)[*str_len] = '\0';
    //
    return 1;
}

// !!! .strnappend
// Append the first VAL_LEN bytes of VAL to *STR
//
// If REALLOC_SZ is > 0 then *STR may be realloc()'ed to a multiple of it to
// fit the appended string.  If it's 0 this will fail if VAL_LEN won't fit.
//
// The string will always be \0 terminated on success and unchanged on failure.
//
// If VAL is NULL but VAL_LEN is > 0 then the realloc/size check is done but
// nothing will be appended.
//
// NOTE:    this is a macro
//
// RETURNS: true if the string was appended, false if not (eg. out of memory,
//          buffer too small etc.)
//
#define strnappend( _s_, _z_, _l_, _r_, _v_, _b_ )  \
        strnapins( (_s_), (_z_), (_l_), (_r_), (_v_), (_b_), 1 )
//+++

// !!! .strnprepend
// Prepend the first VAL_LEN bytes of VAL to the *STR
//
// If REALLOC_SZ is > 0 then *STR may be realloc()'ed to a multiple of it to
// fit the appended string.  If it's 0 this will fail if VAL_LEN won't fit.
//
// The string will always be \0 terminated on success and unchanged on failure.
//
// If VAL is NULL but VAL_LEN is > 0 then the realloc/size check is done but
// nothing will be prepended.
//
// NOTE:    this is a macro
//
// RETURNS: true if the string was prepended, false if not (eg. out of memory,
//          buffer too small etc.)
//
#define strnprepend( _s_, _z_, _l_, _r_, _v_, _b_ )  \
        strnapins( (_s_), (_z_), (_l_), (_r_), (_v_), (_b_), 0 )
//+++

#ifndef STRUTILS_NO_STRSET
// !!! .strset
// Copy as much of S as will fit into D (size Z bytes).  The result is always
// NULL terminated.
//
// This is a safer replacement for strcpy().
//
// SEE:     <strnset()>
//
// NOTE:    this is a macro
//
// RETURNS: true if S fitted, false if not.
//
#define strset( _d_, _z_, _s_ ) \
        strnset( (_d_), (_z_), (_s_), ((_s_) ? strlen( (_s_) ) : 0) )
//+++

// !!! .strnset
// Copy upto STR_LEN bytes of STR into DEST.  If STR_LEN > (DEST_SZ - 1) then
// only as much as will fit will be copied.
//
// The result is always NULL terminated.
//
// If you're not concerned about truncation the return can be ignored.  If
// you are then you need to check it.  Even if the string was truncated it
// will be NULL terminated.
//
// This is a safer version of strncpy() with the differences being always
// \0 terminating and not filling unused space with \0.
//
// RETURNS: true if STR_LEN bytes fitted into DEST_SZ, false if not
//
MAYBE_UNUSED static inline int strnset(
    char *dest,
    size_t dest_sz,
    const char *str,
    size_t str_len
) {
    int fitted;
    fitted = 1;
    if( str_len > (dest_sz - 1) ) {
        str_len = (dest_sz - 1);
        fitted = 0;
    }
    if( str ) {
        strncpy( dest, str, str_len );
    }
    dest[str_len] = '\0';
    return fitted;
}
#endif

#ifndef STRUTILS_NO_STRFROML
// !!! .strfroml
// Get a string representation of VAL.
//
// NOTE:    the returned value is in a static buffer - each call will
//          overwrite the buffer so you'll need to store it somewhere if you
//          want to keep it around.
//
// RETURNS: the string representation of VAL in a static buffer.
//
MAYBE_UNUSED static const char * strfroml(
    long val
) {
static char buf[64];
//
    char *w = (buf + 63);
    *(w--) = '\0';
    //
    if( val == 0 ) {
        *(w--) = '0';
    } else if( val > 0 ) {
        while( val ) {
            *(w--) = ('0' + (val % 10));
            val /= 10;
        }
    } else {
        // % of a -ve number is -ve, we could convert to +ve but
        // - of a -ve is + so can save having to do a multiply.
        while( val ) {
            *(w--) = ('0' - (val % 10));
            val /= 10;
        }
        *(w--) = '-';
    }
    //
    return (w + 1);
}
#endif

#ifndef STRUTILS_NO_CROP
// !!! .strcrop
// Crop STR in-place so it will only contain LEN chars from START.
//
// If START is past the end STR will be set to "", if (START + LEN) is longer
// than STR everything from STR to the \0 will be kept.
//
// STR will not be reallocated since it will never need to be made longer.
//
// RETURNS: true on success, false on error
//
MAYBE_UNUSED static int strcrop(
    char *self,
    size_t start,
    size_t len
) {
    size_t self_len;
    self_len = strlen( self );
    if( start > self_len ) {
        self[0] = '\0';
        return 1;
    }
    //
    if( (start + len) > self_len ) {
        len = (self_len - start);
    }
    memmove( self, (self + start), len );
    self[len] = '\0';
    //
    return 1;
}
#endif

#ifndef STRUTILS_NO_STRTRIM
// !!! .strtrim
// Trim STR in-place
//
// RETURNS: STR
//
MAYBE_UNUSED static char * strtrim(
    char *str
) {
    char *s;
    size_t len;
    //
    len = strlen( str );
    if( !len ) {
        // empty string
        return str;
    }
    //
    // Right trim
    // NOTE:    if "str" is all spaces this will leave it as " "
    while( len > 0 ) {
        if( !(strcws( str[len - 1] )) ) {
            break;
        }
        //
        len -= 1;
        str[len] = '\0';
    }
    //
    // Left trim
    s = str;
    while( strcws( *s ) ) {
        s += 1;
    }
    memmove( str, s, (strlen( s ) + 1) );       // +1 for \0
    //
    return str;
}
#endif

#ifndef STRUTILS_NO_STREQ
// !!! .streq
// Determine if A & B are equal and neither are NULL
//
// NOTE:    this is a macro
//
// RETURNS: true if so, false if not.
//
#define streq( _a_, _b_ ) \
    ((_a_) && (_b_) && (strcmp( (_a_), (_b_) ) == 0))
//+++

// !!! .strneq
// Determine if the first L bytes of A & B are equal and that neither are
// NULL.
//
// NOTE:    this is a macro
//
// RETURNS: true if so, false if not
//
#define strneq( _a_, _b_, _l_ ) \
    ((_a_) && (_b_) && (strcmp( (_a_), (_b_), (_l_) ) == 0))
//+++
#endif

#ifndef STRUTILS_NO_STRBUF
// !!! .strnbuf
// Manages a string buffer (*STR of size *STR_SZ) by reusing it as often
// as possible and free()/malloc()'ing it as needed.
//
// Copy LEN bytes of VAL into *STR.  *STR_SZ is the size of *STR, if (LEN + 1)
// bytes won't fit *STR will be free()'ed and malloc()'ed to (LEN + 1).
//
// If *STR is NULL it will be allocated on the first time this is called and
// *STR_SZ will be set.
//
// When *STR needs to be resized it will be resized in 64byte chunks.
//
// NOTE:    if this fails *STR will be unusable - it will probably have been
//          free()'ed
//
// RETURNS: *STR on success, NULL if malloc() fails.
//
MAYBE_UNUSED static char * strnbuf(
    char **str,
    size_t *str_sz,
    const char *val,
    size_t len
) {
    if( !(*str) || ((len + 1) > *str_sz) ) {
        // need to realloc
        *str_sz = (len + 1);
        *str_sz = (*str_sz + (64 - (*str_sz % 64)));
        free( *str );
        *str = malloc( (*str_sz * sizeof( char )) );
        if( !(*str) ) {
            return NULL;
        }
    }
    //
    memcpy( *str, val, len );
    (*str)[len] = '\0';
    return *str;
}

// !!! .strbuf_next
// Extract from the start of *VAL upto the first instance of SEP and
// update *VAL to point to either after SEP or NULL if there were no more
// SEP's.
//
// The result is stored in the string *STR of size *STR_SZ using <strnbuf()>.
//
// NOTE:    if this fails *STR and *VAL become undefined.
//
// RETURNS: *STR on success, NULL on error
//
MAYBE_UNUSED static char * strbuf_next(
    char **str,
    size_t *str_sz,
    const char **val,
    const char *sep
) {
    const char *br;
    char *out;
    br = strstr( *val, sep );
    if( br ) {
        out = strnbuf( str, str_sz, *val, (br - *val) );
        *val = (br + strlen( sep ));
    } else {
        out = strnbuf( str, str_sz, *val, strlen( *val ) );
        *val = NULL;
    }
    return out;
}
#endif

#ifndef STRUTILS_NO_STRBUILD
// !!! struct strbuild
// STR is the string which will be built up.  SZ is the allocated size of STR,
// LEN is the length of the contents of STR.  GROW_SZ is the block size that
// SZ will be rounded to when STR gets reallocated.
//
// If STR is NULL then it will be allocated on the first use (if SZ is not 0)
// If SZ is 0 STR will be realloc()'ed on the first use (even if not NULL).
// If LEN is 0 then the append will occur at the start.
//
// If GROW_SZ is 0 then STR *MUST* point to a buffer of SZ bytes.  That
// buffer will then be written into upto (SZ - 1) bytes - if the string to
// append will go over SZ then NOTHING is appended - not a partial string.
//
// NOTE:    if GROW_SZ is 0 and STR is NULL then on the first call GROW_SZ
//          will be updated to 1 - the string will be realloc()'ed for each
//          call.  This allows passing a completely empty struct on the first
//          call so it gets filled out.
//
// Don't forget, you can init structs using
//      struct strbuild b = { .grow = 20 };
// (empty members get set to 0), you can also do it later:
//      struct strbuild b;
//      b = (struct strbuild) { .value = &str, .size = sizeof( str ) };
//
// You can also use the old C89 format of:
//      struct strbuild b = { NULL, 0, 0, 20 };
//
// NOTE:    as the "value" member is allocated by the functions you need to
//          free() it if you didn't use a static buffer to start with.
//
typedef struct strbuild strbuild_t;
struct strbuild {
        char *value;
        size_t size;
        size_t length;
        size_t grow;
    };

// !!! .strbuild_append
// Append A to the string in S.  See <strbuild_nappend()> for more information
//
// NOTE:    S must have been inited.
//
// NOTE:    this is a macro
//
// RETURNS: true on success, false on error
//
#define strbuild_append( _s_, _a_ ) \
        strbuild_nappend( (_s_), (_a_), ((_a_) ? strlen( (_a_) ) : 0) )
//+++

// !!! .strbuild_nappend
// Append the first L bytes of V to the string refered to in S
//
// If V is NULL then nothing will be written but the string will be
// allocated/struct setup.
//
// As long as this returns true the resulting string will be \0 terminated.
//
// NOTE:    S must have been inited.
//
// NOTE:    this is a macro
//
// RETURNS: true if the string was appended, false if not (eg. out of memory,
//          buffer too small etc.)
//
#define strbuild_nappend( _s_, _v_, _l_ ) \
    strnappend( \
            &((_s_)->value), &((_s_)->size), &((_s_)->length), (_s_)->grow, \
            (_v_), (_l_) \
        )
//+++

// !!! .strbuild_prepend
// Prepend P to the string in S.  See <strbuild_nprepend()> for more
// information.
//
// NOTE:    S must have been inited.
//
// NOTE:    this is a macro
//
// RETURNS: true on success, false on error
//
#define strbuild_prepend( _s_, _p_ ) \
        strbuild_nprepend( (_s_), (_p_), ((_p_) ? strlen( (_p_) ) : 0) )
//+++

// !!! .strbuild_nprepend
// Prepend the first L bytes of V to the string refered to in S
//
// If V is NULL then nothing will be written but the string will be
// allocated/struct setup.
//
// As long as this returns true the resulting string will be \0 terminated.
//
// NOTE:    S must have been inited.
//
// NOTE:    this is a macro
//
// RETURNS: true if the string was appended, false if not (eg. out of memory,
//          buffer too small etc.)
//
#define strbuild_nprepend( _s_, _v_, _l_ ) \
    strnprepend( \
            &((_s_)->value), &((_s_)->size), &((_s_)->length), (_s_)->grow, \
            (_v_), (_l_) \
        )
//+++
#endif

#ifndef STR_NO_TT
// These are aliases for TT to use "str" as the type.  "strbuild" is valid
// as a type as-is.
//
#define str_trim( _s_ ) strtrim( (_s_) )
#define str_eq( _a_, _b_ ) streq( (_a_), (_b_) )
#define str_neq( _a_, _b_, _l_ ) strneq( (_a_), (_b_), (_l_) )
#endif

#undef MAYBE_UNUSED
#endif

