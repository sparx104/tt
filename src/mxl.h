#ifndef __MXL__
#define __MXL__

typedef struct mxl mxl_t;

mxl_t * mxl_new( void );
void mxl_free( mxl_t * );

int mxl_define( mxl_t *, const char *, const char * );
int mxl_define_l( mxl_t *, const char *, long );
int mxl_define_str( mxl_t *, const char *, size_t );
const char * mxl_get_define( const mxl_t *, const char * );

char * mxl_expand( mxl_t *, const char * );

const char * mxl_get_last_error( const mxl_t * );

#endif