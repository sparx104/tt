//
// tt - Tagged type processor
// Copyright (C) 2017-18 by Chris Dickens
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <unistd.h>

#include "mxl.h"

// Maximum number of definitions (at all scopes)
#define DEFS_SIZE 128

#define SET_ERROR( ... ) snprintf( error_str, 256, __VA_ARGS__ )
#define SHOW_ERROR( _s_ )  \
        fprintf( stderr, error_fmt_str, in_file, line_number, (_s_) )
#define SHOW_LAST_ERROR() SHOW_ERROR( error_str )

#define PRINT( ... ) fprintf( output, __VA_ARGS__ )

// Determine if char is whitespace (and not \0)
#define IS_WS( _a_ ) (((_a_) > 0) && ((_a_) <= 32))

struct def {
        int in_use;
        char type[64];
        char var[64];
        char ins[64];
        char pre_vars[64];
        int pseudo_var;
        int scope_depth;
    };

struct pair {
        struct pair *next;
        char key[256];
        char value[256];
    };

static struct def defs[DEFS_SIZE];  // variable type tags
static struct pair *aliases;        // !@aliases
#ifdef USE_DOT_CALL
static struct pair *passes;         // !@pass variables
#endif
static FILE *output;                // target file pointer (may be stdout)
static char *error_fmt_str;         // formatting string for errors
static char *info_fmt_str;          // formatting string for !@info
static char error_str[256];         // error string buffer
static char warn_str[256];          // warning string buffer
static int enabled_tt;              // 1 if TT processor is enabled
static int enabled_mxl;             // 1 if macros are enabled
static int ignore_next;             // 1 to ignore the next line
static int allow_untagged;          // allow untagged vars
static int warn_untagged;           // warn on untagged vars
static int blocking_for_if;         // 1 if !@if was false, only !@endif
static int collapse_tt;             // 1 to remove ! lines, 0 to insert blank
static int use_auto_scope;          // 1 to try to figure scopes, 0 to disable
static int allow_redefine;          // 1 to allow redefinition (scoped)
static int convert_type_dot_to_us;  // .'s in typename are converted to _
static int trim_indent;             // whether to hide initial indent
static char tag_marker;             // the ! separator
static mxl_t *mxl;                  // macro object

static int tt_pair_add( struct pair **, const char *, const char * );
static const char * tt_pair_get( struct pair *, const char *, const char * );
static void tt_pair_free( struct pair * );

static int tt_read_command_file(
        const char *, const char *, const char *, size_t
    );

#ifdef USE_DOT_CALL
static int tt_is_passed_call( const struct def *, const char * );
#endif

// !!! .tt_dot_to_us.private
// Convert "." to "_" in place
//
// RETURNS: "s"
//
static char * tt_dot_to_us(
    char *s
) {
    char *os;
    os = s;
    while( *s ) {
        if( *s == '.' ) {
            *s = '_';
        }
        s += 1;
    }
    return os;
}

// !!! .tt_to_bool.private
// Convert the string into a bool value
//
// The value is put into "val_out".  If this returns FALSE to indicate an
// error it will set "val_out" to 0 so if you don't actually need an
// explicit "off" value you can ignore the return.
//
// If "expr" is NULL then "(empty)" is used
//
// If "name" is given then an invalid value will result in the error being
// set to "'<expr'> is not a valid value for <name>" when returning FALSE
//
// RETURNS: TRUE if the string was valid (and sets val_out), FALSE if not
//
static int tt_to_bool(
    const char *expr,
    int *val_out,
    const char *name
) {
    if( !expr ) {
        expr = "(empty)";
    }
    if( strstr( "on;1;enable;enabled;true;yes", expr ) ) {
        *val_out = 1;
        return 1;
    } else if( strstr( "off;0;disable;disabled;false;no", expr ) ) {
        *val_out = 0;
        return 1;
    } else {
        if( name ) {
            SET_ERROR( "'%s' is not a valid value for %s", expr, name );
        }
        *val_out = 0;
        return 0;
    }
}

// !!! .tt_find_end_bracket.private
// Search for the ending ) taking nesting into account.
//
// NOTE: STR should not include the opening (
//
// NOTE:    the returned value is not const because str may not be.
//          ENSURE CONST SAFETY
//
// RETURNS: pointer to the ending ) or NULL if not found
//
static char *tt_find_end_bracket(
    const char *str
) {
    int b_depth = 0, in_q = 0;
    //
    while( *str ) {
        switch( *str ) {
            case '(':
                if( !in_q ) {
                    b_depth += 1;
                }
                break;
                //
            case ')':
                if( !in_q ) {
                    if( b_depth == 0 ) {
                        return (char *)str;
                    }
                    b_depth -= 1;
                }
                break;
                //
            case '"':
                in_q = !in_q;
                break;
                //
            case '\\':
                // skip the next char
                str += 1;
                break;
        }
        str += 1;
    }
    //
    // Ran out of string without finding )
    return NULL;
}

// !!! .tt_has_args.private
// Determine if the next non-whitespace char is not ) to indicate that
// this call has arguments or not
//
// RETURNS: true if the call has arguments, false if not
//
static int tt_has_args(
    char *s
) {
    while( *s ) {
        if( IS_WS( *s ) ) {
            // whitespace does nothing
        } else if( *s == ')' ) {
            // if we see this we've got no args
            return 0;
        } else {
            // anything not whitespace or ) means we have args
            return 1;
        }
        s += 1;
    }
    //
    // This happens if the string ends after the (, this most likely means
    // that the string is split across lines so we'll assume we have args
    return 1;
}


// !!! .tt_add_definition.private
// Add a type definition.  "type" is the function call prefix (without the
// last _).  "var" is the variable name and "pre_vars" (if not NULL) contains
// any variable string to add before the resolvee variable
//
// Eg, "<type>_<call>( [<pre_vars>,] <var>...."
//
// "scope_depth" is stored for <tt_purge_definitions()> to remove definitions
// when they go out of scope
//
// RETURNS: the (struct def) we added or NULL on error
//
static struct def * tt_add_definition(
    const char *type,
    const char *var,
    const char *pre_vars,
    int scope_depth
) {
    struct def *d = NULL;
    int idx;
    //
    if( !allow_redefine ) {
        // search for an existing definition at this scope depth, if found
        // we error as we're not allowed to redefine now
        for( idx = 0; idx < DEFS_SIZE; idx++ ) {
            if( defs[idx].in_use ) {
                if( defs[idx].scope_depth == scope_depth ) {
                    if( strcmp( defs[idx].var, var ) == 0 ) {
                        // we already have this var
                        SET_ERROR( "Redefinition of %s\n", var );
                        return NULL;
                    }
                }
            }
        }
    }
    //
    // Find an empty slot
    for( idx = 0; idx < DEFS_SIZE; idx++ ) {
        if( !(defs[idx].in_use) ) {
            d = &(defs[idx]);
            break;
        }
    }
    //
    if( !d ) {
        // no slots
        SET_ERROR( "Too many definitions" );
        return NULL;
    }
    //
    // Setup the definition
    memset( d, 0, sizeof( struct def ) );
    d->in_use = 1;
    strncpy( d->type, tt_pair_get( aliases, type, type ), 64 );
    d->type[63] = '\0';
    //
    if( *var == '~' ) {
        // this is a pseudo declaration - no variable
        d->pseudo_var = 1;
        var += 1;
    } else {
        // standard declaration - variable name will be included
        d->pseudo_var = 0;
    }
    //
    strncpy( d->var, var, 64 );
    d->var[63] = '\0';
    //
    // You can specify that a different variable be inserted for this
    // variable by including it after "=" in the name, eg. "x=y" will
    // insert the variable "y" when "x" is used.
    //
    // This simulates casting (eg. "!parts str_p=str").  It is the
    // same as "!parts(str) ~str_p".
    //
    // NOTE: you only have 63 chars for the whole definition
    char *repl;
    repl = strchr( d->var, '=' );
    if( repl ) {
        *(repl++) = '\0';
        strncpy( d->ins, repl, 64 );
    } else {
        strncpy( d->ins, d->var, 64 );
    }
    d->ins[63] = '\0';
    //
    if( pre_vars ) {
        strncpy( d->pre_vars, pre_vars, 64 );
        d->pre_vars[63] = '\0';
    } else {
        d->pre_vars[0] = '\0';
    }
    d->scope_depth = scope_depth;
    //
    return d;
}

// !!! .tt_purge_definitions.private
// Delete (mark unused) any variables with the given scope depth
//
void tt_purge_definitions(
    int scope_depth
) {
    int idx;
    for( idx = 0; idx < DEFS_SIZE; idx++ ) {
        if( defs[idx].in_use ) {
            if( defs[idx].scope_depth == scope_depth ) {
                defs[idx].in_use = 0;
            }
        }
    }
}

// !!! .tt_load_definition.private
// Load a "!type[(parent)] var[, var...]" string
//
// "line" must not include the initial "!".
//
// On encountering ! this will call itself again, this allows multiple
// items per line, eg. "!string name !int x".  If there are such multiple
// definitions the return value will be the last added or NULL if any fail.
//
// For the sake of consistency ; and : are ignored.
//
// NOTE: "line" is modified
//
// RETURNS: true on success or false on error (and sets error_str)
//
static int tt_load_definition(
    char *line,
    int scope_depth
) {
    char *var;
    //
    if( line[0] == '!' ) {
        line += 1;
    }
    //
    var = strchr( line, ' ' );
    if( !var ) {
        // invalid definition
        SET_ERROR( "Invalid definition (no variables): %s", line );
        return 0;
    }
    //
    // "!type(prevars)" is available to pass extra variables, we deal with
    // that
    char *pre_vars;
    pre_vars = strchr( line, '(' );
    if( pre_vars ) {
        *(pre_vars++) = '\0';
//        var = strchr( pre_vars, ')' );
        var = tt_find_end_bracket( pre_vars );
        if( !var ) {
            // invalid
            SET_ERROR( "Invalid definition (unterminated parent)" );
            return 0;
        }
    }
    //
    // Terminate before the vars, set the var position and convert . to _
    *(var++) = '\0';
    if( convert_type_dot_to_us ) {
        tt_dot_to_us( line );
    }
    //
    int running;
    size_t word_len;
    char word[256];
    word_len = 0;
    running = 1;
    while( running ) {
        switch( *var ) {
            case '!':
            case ',':
            case ';':
            case ':':
            case '\0':
                word[word_len] = '\0';
                if( !tt_add_definition(
                        line, word, pre_vars, scope_depth
                    ) ) {
                    // Error will be set by call
                    return 0;
                }
                if( *var == '\0' ) {
                    running = 0;
                }
                word_len = 0;
                //
                // Start a new type?
                if( *var == '!' ) {
                    return tt_load_definition( (var + 1), scope_depth );
                }
                break;
                //
            case ' ':
                if( word_len == 0 ) {
                    break;
                }
                // falls through
            default:
                if( word_len < 256 ) {
                    word[word_len++] = *var;
                }
        }
        var += 1;
    }
    //
    return 1;
}

// !!! .tt_get_def_for_var.private
// Get the type for a variable
//
// This uses all scopes so parent scopes will be looked in.  If multiple
// scopes contain the name the type from the innermost scope is used
//
// NOTE:    this also resolves aliases for the type name
//
// RETURNS: the type or NULL if not found
//
static const struct def * tt_get_def_for_var(
    const char *var
) {
//static char var_buf[256];
    //
    struct def *sel;
    int idx, scope = -1;
    //
    // If the variable starts * or & (eg. &x.test( 123 ) we will not use the
    // */& to lookup the name.
    if( (*var == '&') || (*var == '*') ) {
        var += 1;
    }
    //   
    sel = NULL;
    for( idx = 0; idx < DEFS_SIZE; idx++ ) {
        if( defs[idx].in_use ) {
            if( strcmp( defs[idx].var, var ) == 0 ) {
                // matching variable name, we only store if a deeper scope
                // than the one we have
                if( defs[idx].scope_depth >= scope ) {
                    sel = &(defs[idx]);
                    scope = sel->scope_depth;
                }
            }
        }
    }
    //
    return sel;
}

// !!! .tt_resolve_call.private
// Translate dot call into static call
//
// If HAS_ARGS is true then we've something between the ().
// If HAS_SPACE is true then it was "call (" instead of "call("
//
static int tt_resolve_call(
    char *word,
    int has_args,
    int has_space,
    const char *in_file,
    size_t line_number
) {
    const struct def *def;
    char *call;
    int no_self;
    //
    if( *word == ')' ) {
        // is part of a chain (eg. x.call().then()).  We can't handle these
        // so let the host deal with it (will either work [js] or not [c])
        //
        goto write_output;
    }
    //
    // Some special strings break things, deal with them now
    if( strstr( word, "..." ) ) {
        goto write_output;
    }
    //
#ifdef USE_DOT_SEP
    call = strrchr( word, '.' );
#else
    while( *word == '!' ) {
        // starts ! (eg. !x!is_empty() )
        // output however many !'s there are then continue from the
        // first non-!
        PRINT( "!" );
        word += 1;
    }
    call = strrchr( word, tag_marker );
#endif
    if( call ) {
        // terminate "word" at the .
        *(call++) = '\0';
        //
        def = tt_get_def_for_var( word );
        if( def ) {
            // have a tag
            if(
                    (strcmp( def->type, "-" ) == 0) ||
                    (strcmp( def->type, "any" ) == 0) ||
                    (strcmp( def->type, "none" ) == 0)
                ) {
                // Type is "-" or "any", this means we leave any
                // calls to this variable as-is.
                //
                // Put the marker char back
                *(call - 1) = tag_marker;
                goto write_output;
            }
            //
#ifdef USE_DOT_SEP
            if( tt_is_passed_call( def, call ) ) {
                // call is allowed as is (eg. x.toLowerCase() for js)
                // these are defined with !@pass <type> <call> (often
                // via !@include)
                *(call - 1) = tag_marker;
                goto write_output;
            }
#endif
            //
            no_self = 0;
            if( *call == '~' ) {
                // if call starts ~ then var won't be inserted as "self"
                no_self = 1;
                call += 1;
            }
            //
            // Write the call function
            PRINT( "%s_%s( ", def->type, call );
            if( *(def->pre_vars) ) {
                PRINT( "%s", def->pre_vars );
                if( has_args || !(def->pseudo_var) ) {
                    // will be inserting args or ourself
                    PRINT( ", " );
                }
            }
            //
            // Write this variable (if not a pseudo var) along with any prefix
            if( !(def->pseudo_var) && !no_self ) {
                // write out any call-size prefix to the given var and
                // advance to the first AZaz09 char in the name
                // (eg. & from "&x.test()" )
                while( 1 ) {
                    if( 
                            ((*word >= 'a') && (*word <= 'z')) ||
                            ((*word >= 'A') && (*word <= 'Z')) ||
                            ((*word >= '0') && (*word <= '9'))
                        ) {
                        // is azAz09 so modifiers are all written
                        break;
                    }
                    PRINT( "%c", *word );
                    word += 1;
                }
                //
                PRINT( "%s%c", def->ins, (has_args ? ',' : ' ') );
            }
            return 1;
        } else {
            // no tag
            if( allow_untagged ) {
                // language allows x.y() calls so we can't be sure this is
                // actually untyped so we just let it go and let the runtime/
                // compiler deal with it
                //
                // Put the separator (!/.) back
                if( warn_untagged ) {
                    // Warn on untagged variables but don't quit
                    snprintf( error_str, 256, "'%s' is untyped", word );
                    fprintf(
                            stderr, info_fmt_str,
                            in_file, line_number, error_str
                        );
                    *error_str = '\0';
                }
                //
#ifdef USE_DOT_SEP
                *(call - 1) = '.';
#else
                *(call - 1) = tag_marker;
#endif
                //
            } else {
                // variable not typed
                snprintf( error_str, 256, "'%s' is untyped", word );
                return 0;
            }
        }
    }
    //
    // No separator so must be a normal call
    //
write_output:
    if( has_space ) {
        PRINT( "%s (", word );
    } else {
        PRINT( "%s(", word );
    }
    return 1;
}

// !!! .tt_pair_add.private
// Add a pair to the given head
//
static int tt_pair_add(
    struct pair **head,
    const char *key,
    const char *value
) {
    struct pair *a;
    a = malloc( sizeof( struct pair ) );
    if( !a ) {
        return 0;
    }
    strncpy( a->key, key, 256 );
    a->key[255] = '\0';
    strncpy( a->value, value, 256 );
    a->value[255] = '\0';
    //
    a->next = *head;
    *head = a;
    return 1;
}

// !!! .tt_pair_get_l.private
// If there is a child pair in the list starting with "head" who's
// key matches the first "key_len" chars of "key" return that.
// If not, return "def" (which can be NULL)
//
// RETURNS: the matching value or "def" if not found
//
static const char * tt_pair_get_l(
    struct pair *head,
    const char *key,
    size_t key_len,
    const char *def
) {
    while( head ) {
        if( strncmp( head->key, key, key_len ) == 0 ) {
            return head->value;
        }
        head = head->next;
    }
    return def;
}

// !!! .tt_pair_get.private
// If there is a child pair in the list starting with "head" who's
// key matches return that.  If not, return "def" (which can be NULL)
//
// RETURNS: the matching value or "def" if not found
//
static const char * tt_pair_get(
    struct pair *head,
    const char *key,
    const char *def
) {
    if( !key ) {
        return NULL;
    }
    return tt_pair_get_l( head, key, strlen( key ), def );
}

// !!! .tt_pair_free.private
// Free the pair and all subsequent ones.
//
// NOTE:    don't forget to NULL the value of "head"
//
static void tt_pair_free(
    struct pair *head
) {
    struct pair *next;
    while( head ) {
        next = head->next;
        free( head );
        head = next;
    }
}

// !!! .tt_load_command.private
// Perform a command "!@cmd [arg]" action
//
// If LINE contains the initial !@ it'll be removed.
//
// NOTE:    LINE *IS MODIFIED* - at the least it'll have the argument
//          removed.
//
// RETURNS: TRUE on success or FALSE and sets error on error
//
static int tt_load_command(
//    const char *command,
    char *line,
    const char *in_file,
    size_t line_number,
    int scope_depth
) {
#define LTRIM( _s_ ) while( IS_WS( *(_s_) ) ) (_s_)++;
#define IS_COMMAND( _c_ ) (strcmp( line, (_c_) ) == 0)
static char buf[256];
    //
    (void) scope_depth;
    //
    char *arg, *arg1, *tmp;
    const char *val;
    int bool, if_chk;
    size_t len;
    //
    // remove any leading !@ and trailing ;
    if( (line[0] == '!') && (line[1] == '@') ) {
        line += 2;
    }
    len = strlen( line );
    if( len > 0 ) {
        if( line[len - 1] == ';' ) {
            line[len - 1] = '\0';
        }
    }
    //
    if( line[0] == '\0' ) {
        // empty line, just ignore
        return 1;
    }
    //
    arg = strchr( line, ' ' );
    if( arg ) {
        *(arg++) = '\0';
        LTRIM( arg );
    } else {
        arg = (line + strlen( line ));
    }
    //
    if( IS_COMMAND( "disable" ) ) {
        // !!! disable [tt] [mx|replace] [script]
        // If no arguments uses "tt mx"
        if( !(*arg) || strstr( arg, "tt" ) ) {
            enabled_tt = 0;
        }
        if( !(*arg) || strstr( arg, "mx" ) || strstr( arg, "replace" ) ) {
            enabled_mxl = 0;
        }
        //
    } else if( IS_COMMAND( "enable" ) ) {
        // !!! enable [tt] [mx|replace] [script]
        // If no arguments uses "tt mx"
        if( !(*arg) || strstr( arg, "tt" ) ) {
            enabled_tt = 1;
        }
        if( !(*arg) || strstr( arg, "mx" ) || strstr( arg, "replace" ) ) {
            enabled_mxl = 1;
        }
        //
    } else if( IS_COMMAND( "collapse" ) ) {
        // !!! collapse <bool>
        // Whether to collapse ! lines and remove them (on) or leave as blank
        // lines ("off").  Default is "off" to leave the lines
        return tt_to_bool( arg, &collapse_tt, "collapse" );
        //
    } else if( IS_COMMAND( "tag-marker" ) ) {
        // !!! tag-marker <str>
        // By default ! is used to invoke a function on a tagged variable
        // eg. "x!test()", this allows you to replace it.  Only the first
        // character of <str> will be used
        tag_marker = (*arg ? *arg : '!');
        return 1;
        //
    } else if( IS_COMMAND( "ignore" ) ) {
        // !!! ignore
        // ignore the next line, works around any non-fixable issues
        ignore_next = 1;
        //
    } else if( IS_COMMAND( "alias" ) ) {
        // !!! alias.<name> <original>
        arg1 = strpbrk( arg, " =" );
        if( arg1 ) {
            *(arg1++) = '\0';
            LTRIM( arg1 );
            if( tt_pair_add( &aliases, arg, arg1 ) ) {
                goto command_ok;
            }
        }
        SET_ERROR( "Unable to add alias" );
        return 0;
        //
    } else if( IS_COMMAND( "define" ) ) {
        // !!! define.<name> <replace>
        // "[[name]]" will then be replaced with "replace"
        //
        // If REPLACE contains "%%" then the argument given during expansion
        // will replace it when expanding.  This is recursive so can be nested.
        arg1 = strpbrk( arg, " =" );
        if( arg1 ) {
            *(arg1++) = '\0';
            LTRIM( arg1 );
            if( mxl_define( mxl, arg, arg1 ) ) {
                goto command_ok;
            }
        }
        SET_ERROR( "Unable to add define" );
        return 0;
        //
    } else if( IS_COMMAND( "info" ) ) {
        // !!! info <message>
        //
        // Display "message" to stderr, doesn't fail
        fprintf( stderr, info_fmt_str, in_file, line_number, arg );
        //
    } else if( IS_COMMAND( "allow-untagged" ) ) {
        // !!! allow-untagged <bool>
        //
        // Normally, any x.y() call where "x" doesn't have a type tag will
        // fail.  If this is enabled it will be output as is.
        //
        // Useful for languages which allow . calls.
        //
        // Use "warn" to allow variables but warn when they show up - you can
        // then supress the warnings with !-
        //
        // You can specify a single variable to be untagged with !- x no
        // matter what this setting is.
        //
        warn_untagged = 0;
        return tt_to_bool( arg, &allow_untagged, "allow-untagged" );
        //
    } else if( IS_COMMAND( "warn-untagged" ) ) {
        // !!! warn-untagged <bool>
        // Warn on untagged variables but allow them (automatically sets
        // "allow-untagged" to "on")
        //
        // You can suppress warnings by tagging variables with !-
        //
        // SEE: <allow-untagged>
        //
        if( tt_to_bool( arg, &warn_untagged, "warn-untagged" ) ) {
            if( warn_untagged ) {
                allow_untagged = 1;
            }
            return 1;
        } else {
            return 0;
        }
        //
    } else if( IS_COMMAND( "include" ) ) {
        // !!! include <file>
        //
        // Run the commands in <file>
        //
        strncpy( buf, in_file, 256 );
        buf[255] = 0;
        tmp = strrchr( buf, '/' );
        if( tmp ) {
            *tmp = '\0';
        } else {
            buf[0] = '.';
            buf[1] = '\0';
        }
        if( !tt_read_command_file( arg, buf, in_file, line_number ) ) {
            // will have set error
            return 0;
        }
        //
    } else if( IS_COMMAND( "auto-scope" ) ) {
        // !!! auto-scope on|off
        //
        // Enable or disable the automatic scoping.  If on then any { will
        // open a scope and any } will close it.  If off then you need to
        // manually mark scopes with "!{" and "!}"
        //
        // Default is on
        //
        return tt_to_bool( arg, &use_auto_scope, "auto-scope" );
        //
    } else if( IS_COMMAND( "allow-redefine" ) ) {
        // !!! allow-redefine on|off
        //
        // Enable or disable whether variables can have their type changed
        // within the same scope.
        //
        // Default is on
        //
        return tt_to_bool( arg, &allow_redefine, "allow-redefine" );
        //
    } else if( IS_COMMAND( "if" ) || IS_COMMAND( "if-not" ) ) {
        // !!! if EXPR 
        // !!! if-not EXPR 
        // If EXPR is bool:true then the following lines up to !@endif will
        // be left, if bool:false then they will be removed - even commands
        //
        // If EXPR starts ! then the lines will be removed if it's
        // bool:true and left as-is if bool:false.
        if( strcmp( line, "if" ) == 0 ) {
            if_chk = 1;
        } else {
            if_chk = 0;
        }
        val = mxl_get_define( mxl, arg );
        if( !val ) {
            SET_ERROR( "%s: No such define '%s'", line, arg );
            return 0;
        }
        if( tt_to_bool( val, &bool, "an if conditional" ) ) {
            blocking_for_if = (bool != if_chk);
        } else {
            return 0;
        }
        //
    } else if( IS_COMMAND( "endif" ) ) {
        // !!! endif
        // End an !@if block.
        //
        // We actually do nothing - the main parse loop actually acts on this
        return 1;
        //
#ifdef USE_DOT_SEP
    } else if( IS_COMMAND( "pass" ) ) {
        // !!! pass <type>.<call>
        // Define a type/call pair which will never be translated
        //
        strncpy( buf, arg, 256 );
        buf[255] = '\0';
        tmp = strchr( buf, '.' );
        if( tmp ) {
            *(tmp++) = '\0';
            if( !tt_pair_add( &passes, buf, tmp ) ) {
                SET_ERROR( "Unable to add pass" );
                return 0;
            }
        } else {
            SET_ERROR( "Invalid pass definition" );
            return 0;
        }
        //
#endif
    } else if( IS_COMMAND( "trim-indent" ) ) {
        // !!! trim-indent on|off
        // If on then all lines have their initial indent removed, if off
        // then the initial indent is preserved.
        //
        // Default is OFF
        //
        return tt_to_bool( arg, &trim_indent, "trim-indent" );
        //
    } else {
        SET_ERROR( "Unknown command '%s'", line );
        return 0;
    }
    //
command_ok:
    return 1;
    //
#undef LTRIM
}

// !!! .tt_read_command_file.private
// Load the commands from the given file
//
// If "rel" is not NULL then it'll be used as the origin path, if NULL
// cwd is used.
//
// NOTE:    if "rel" is NULL then TRUE will be returned if the file
//          doesn't exist.  If not NULL then the file not existing is an
//          error
//
// RETURNS: TRUE on success, FALSE on error
//
static int tt_read_command_file(
    const char *path,
    const char *rel,
    const char *in_file,
    size_t in_line_number
) {
static char buf[256];
    //
    FILE *f;
    if( path ) {
        if( !rel ) {
            f = fopen( path, "r" );
            if( !f ) {
                return 1;
            }
        } else {
            if( *rel != '/' ) {
                snprintf( buf, 256, "%s/%s", rel, path );
                path = buf;
            }
            f = fopen( path, "r" );
            if( !f ) {
                SET_ERROR( "Cannot include '%s'", path );
                return 0;
            }
        }
    } else {
        // NULL path is stdin
        f = stdin;
    }
    //
    // Read each line, any not starting "#" or "" will be used as commands
    char *nl, *line;
    while( fgets( buf, 256, f ) ) {
        nl = strchr( buf, '\n' );
        if( nl ) {
            // we don't bother warning if the line is too long, you'll
            // find out when it's used...
            *nl = '\0';
        }
        //
        // trim
        line = buf;
        while( IS_WS( *line ) ) {
            line += 1;
        }
        if( *line == '!' ) {
            // remove ! and !@ at the start
            if( *(line + 1) == '@' ) {
                line += 2;
            } else {
                line += 1;
            }
        }
        if( *line && (*line != '#') ) {
            // not a comment
            if( !tt_load_command( line, in_file, in_line_number, 0 ) ) {
                // error already set
                return 0;
            }
        }
    }
    if( path ) {
        fclose( f );
    }
    return 1;
}

#ifdef USE_DOT_SEP
// !!! .tt_is_passed_call.private
// In some cases (eg. JS) some . calls are in the language/stdlib.  These
// can be marked with !@pass [<type>].<call> to allow them untyped.
//
// NOTE: If "type" is omitted (the . is needed) then all calls with that
//       name will be passed through (must still be typed if not ignored)
//
static int tt_is_passed_call(
    const struct def *def,
    const char *call
) {
    struct pair *p = passes;
    while( p ) {
        if( (strcmp( p->key, def->type ) == 0) || !(p->key[0]) ) {
            // matching type (or was defined as ".func")
            if( strcmp( p->value, call ) == 0 ) {
                return 1;
            }
        }
        p = p->next;
    }
    return 0;
}
#endif

// !!! .main
int main(
    int argc,
    char **argv
) {
#define BUF_SIZE 1024
    //
    if( isatty( fileno( stdout ) ) ) {
        // can use colours
        error_fmt_str = "[tt] \033[1m\033[34m%s:%zu - \033[31m%s\033[0m\n";
        info_fmt_str = "[tt] \033[1m\033[34m%s:%zu - \033[33m%s\033[0m\n";
    } else {
        // piped somewhere, no ANSI
        error_fmt_str = "[tt] %s:%zu - %s\n";
        info_fmt_str = "[tt] %s:%zu - %s\n";
    }
    //
    char buf[BUF_SIZE];
    const char *in_file, *out_file, *conf_file;
    int i;
    //
    // Look through the args, find the first 3 that don't start -, these
    // are in, out and conf respecively
    in_file = NULL;
    out_file = NULL;
    conf_file = NULL;
    for( i = 1; i < argc; i++ ) {
        if( argv[i][0] == '-' ) {
            continue;
        }
        //
        if( !in_file ) {
            in_file = argv[i];
        } else {
            if( !out_file ) {
                out_file = argv[i];
            } else {
                if( !conf_file ) {
                    conf_file = argv[i];
                } else {
                    fprintf(
                            stderr, "Too many filenames in argument %d\n", i
                        );
                    return 2;
                }
            }
        }
    }
    //
    if( !in_file ) {
        if( argc == 1 ) {
            printf(
                    "tt - Tagged type preprocessor\n"
                    "Copyright, (c)2017-18 by Chris Dickens\n"
                    "\n"
                    "This program comes with ABSOLUTELY NO WARRANTY.\n"
                    "This is free software, and you are welcome to\n"
                    "redistribute it under certain conditions.\n"
                    "\n"
                    "More information: https://gitlab.com/sparx104/tt\n"
                    "\n"
                );
        }
        fprintf(
                ((argc == 1) ? stdout : stderr),
                "Use: %s <in> [<out> [<config>]] [-Dkey[=val]...]\n"
                "     IN        - input file, can be '-' for stdin\n"
                "     OUT       - output file, can be '-' for stdout\n"
                "     CONFIG    - additional configuration, can be '-' for \n"
                "                 stdin, as long as IN is not '-'\n"
                "     -Dkey     - defines 'key' to be '1'\n"
                "     -Dkey=val - defines 'key' to be 'val'\n",
                argv[0]
            );
        return 2;
    }
    //
    if( !out_file ) {
        out_file = "-";
    }
    //
    FILE *f;
    if( strcmp( in_file, "-" ) != 0 ) {
        // we have a filename
        f = fopen( in_file, "r" );
        if( !f ) {
            fprintf( stderr, "[tt] Unable to read %s\n", in_file );
            return 1;
        }
    } else {
        // "-" is stdin
        f = stdin;
    }
    //
    // Deal with output target
    if( strcmp( out_file, "-" ) != 0 ) {
        // to file
        if( strcmp( in_file, out_file ) == 0 ) {
            fprintf( stderr, "[tt] Input and output cannot be the same\n" );
            return 1;
        }
        //
        output = fopen( out_file, "w" );
        if( !output ) {
            fprintf( stderr, "[tt] Unable to write to %s\n", out_file );
            return 1;
        }
    } else {
        // to stdout
        output = stdout;
    }
    //
    // !!! main.setup
    // Read and parse
    aliases = NULL;
    ignore_next = 0;
    blocking_for_if = 0;
    //
    // Defaults
    enabled_tt = 1;                     // !@enable tt
    enabled_mxl = 1;                    // !@enable mx|replace
    allow_untagged = 0;                 // !@allow-untagged off
    collapse_tt = 0;                    // !@collapse off
    use_auto_scope = 1;                 // !@auto-scope on
    allow_redefine = 1;                 // !@allow-redefine on
    convert_type_dot_to_us = 0;         // TODO: implement command if wanted
    tag_marker = '.';                   // !@tag-marker
    trim_indent = 0;                    // !@trim-indent off
    //
    // Setup macro engine and add some default definitions
    mxl = mxl_new();
    mxl_define( mxl, "__file__", in_file );
    mxl_define( mxl, "__output__", out_file );
    // "__line__" is calculated in tt_replace()
    //
    // Try loading .ttrc, returns TRUE if ok or didn't exist
    if( !tt_read_command_file( ".ttrc", NULL, ".ttrc", 0 ) ) {
        // error set
        fprintf( stderr, error_fmt_str, ".ttrc", 0, error_str );
        return 1;
    }
    //
    // If we were given a 3rd argument we'll read the config from that too
    int ok;
    if( conf_file ) {
        if( strcmp( conf_file, "-") == 0 ) {
            // If the config is "-" and input isn't we'll read from stdin
            if( f == stdin ) {
                // can't read input and config from stdin
                SET_ERROR( "Config and input cannot both be stdin" );
                ok = 0;
            } else {
                ok = tt_read_command_file( NULL, NULL, "stdin", 0 );
            }
        } else {
            ok = tt_read_command_file( conf_file, NULL, "stdin", 0 );
        }
        //
        if( !ok ) {
            fprintf( stderr, error_fmt_str, conf_file, 0, error_str );
            return 1;
        }
    }
    //
    // Finally, load any defines from the commandline, these are in the
    // format -Dkey[=value]
    char *arg;
    for( i = 1; i < argc; i++ ) {
        arg = argv[i];
        if( (arg[0] == '-') && (arg[1] == 'D') ) {
            arg += 2;
            if( strchr( arg, '=' ) ) {
                mxl_define_str( mxl, arg, strlen( arg ) );
            } else {
                mxl_define( mxl, arg, "1" );
            }
        }
    }
    //
    // !!! main.parse
    size_t line_number;
    char cmp_buf[256], word_buf[256];
    char c, *line, *cmp, *word;
    size_t len, repl_len;
    int in_q, scope_depth, exit_result, force_mxl;
    char *nl, q, *repl_line;
    //
    in_q = 0;
    q = '\0';
    line_number = 0;
    scope_depth = 0;
    force_mxl = 0;
    exit_result = 0;
    while( fgets( buf, BUF_SIZE, f ) ) {
        nl = strchr( buf, '\n' );
        if( nl ) {
            // have a newline so complete line.  Remove the \n
            *nl = '\0';
        } else if( strlen( buf ) == (BUF_SIZE - 1) ) {
            // don't have a new line and have max chars so is too long
            SHOW_ERROR( "Line too long" );
            exit_result = 1;
            goto finish;
        } else {
            // no newline but not too long - is the last line in the file
            // and file doesn't end \n
        }
        //
        line = buf;
        cmp = cmp_buf;
        word = word_buf;
        *word = '\0';
        //
        line_number += 1;
        //
        // skip the initial indent
        while( IS_WS( *line ) ) {
            if( trim_indent ) {
                // trim without outputting
                line += 1;
            } else {
                // trim and output the trimmed characters
                PRINT( "%c", *(line++) );
            }
        }
        //
        // And the last
        len = strlen( line );
        while( IS_WS( line[len] ) && (len > 0) ) {
            line[len--] = '\0';
        }
        //
        // Check to see if we're enabled (via !@if) - !@ignore/!@enable
        // comes after this.  We have to handle !@endif ourselves in a
        // special way else.
        if( blocking_for_if ) {
            if( strcmp( line, "!@endif" ) == 0 ) {
                // go back to normal after this
                blocking_for_if = 0;
            }
            continue;
        }
        //
        if( *line == '!' ) {
            // If line starts ! then it's a declaration, command or manual
            // scope
            switch( *(line + 1) ) {
                case '#':
                    // comment, we don't include the content and will
                    // collapse the line if enabled
                    ok = 1;
                    break;
                    //
                case '@':
                    // is a command
                    ok = tt_load_command(
                            (line + 2),
                            in_file, line_number, scope_depth
                        );
                    break;
                    //
                case '{':
                    // manually open scope
                    scope_depth += 1;
                    ok = 1;
                    break;
                    //
                case '}':
                    // manually close scope
                    if( scope_depth > 0 ) {
                        // purge any definitions from the current scope
                        tt_purge_definitions( scope_depth );
                        scope_depth -= 1;
                        ok = 1;
                    } else {
                        // already in lowest scope, since this is manual
                        // we fail.
                        SET_ERROR( "Cannot close base scope" );
                        ok = 0;
                    }
                    break;
                    //
                case ':':
                    // expand this as a macro
                    len = strlen( line );
                    if( len >= (BUF_SIZE + 3) ) {
                        SET_ERROR( "Line too long" );
                        ok = 0;
                    }
                    // Overwrite the !: with [[ and add ]] to the end
                    line[0] = '[';
                    line[1] = '[';
                    line[len++] = ']';
                    line[len++] = ']';
                    line[len++] = '\0';
                    force_mxl = 1;
                    break;
                    //
                default:
                    // must be a type definition
                    ok = tt_load_definition( (line + 1), scope_depth );
            }
            //
            if( ok ) {
                // insert blank line to ensure src/dest line up
                if( !collapse_tt ) {
                    PRINT( "\n" );
                }
            } else {
                SHOW_LAST_ERROR();
                exit_result = 1;
                break;
            }
            if( !force_mxl ) {
                // don't process the line further unless this was a macro
                // in which case it's been converted to [[x]] and needs to
                // be run
                continue;
            }
        }
        //
        if( ignore_next ) {
            // ignore this line then continue as normal
            ignore_next = 0;
            PRINT( "%s\n", line );
            continue;
        }
        //
        // Do define expansion if on, have defines and line contains "["
        // Or if forced to expand this line as a command via !:
        if( force_mxl || (enabled_mxl && strchr( line, '[' )) ) {
            mxl_define_l( mxl, "__line__", (long)line_number );
            repl_line = mxl_expand( mxl, line );
            if( !repl_line ) {
                SET_ERROR( "%s", mxl_get_last_error( mxl ) );
                SHOW_LAST_ERROR();
                exit_result = 1;
                goto finish;
            }
            //
            repl_len = strlen( repl_line );
            if( repl_len > (BUF_SIZE - 1) ) {
                SET_ERROR( "Replacement too long" );
                SHOW_LAST_ERROR();
                exit_result = 1;
                goto finish;
            }
            memcpy( buf, repl_line, repl_len );
            buf[repl_len] = '\0';
            line = buf;
            free( repl_line );
            //
            if( force_mxl ) {
                // if called by "!:macro" we won't TT the line
                force_mxl = 0;
                PRINT( "%s\n", line );
                continue;
            }
        }
        //
        if( !enabled_tt ) {
            // not enabled so just write the line and continue
            PRINT( "%s\n", line );
            continue;
        }
        //
        // We need to parse this as it must be a normal line
        while( *line ) {
            c = *(line++);
            //
            if( in_q ) {
                switch( c ) {
                    case '\\':
                        *(word++) = '\\';
                        c = *(line++);
                        break;
                        //
                    case '"':
                    case '\'':
                        if( c == q ) {
                            in_q = 0;
                        }
                        break;
                }
                *(word++) = c;
            } else {
                if( (c == '/') && (*line == '/') ) {
                    // inline comment, dump the rest of the line and continue
                    // this is to stop scopes/quotes in such comments breaking
                    // things
                    //
                    // NOTE:    if there's no space before the // then the
                    //          previous word is written out as is - NOT
                    //          RESOLVED.  Always put spaces before //
                    //
                    // NOTE:    no newline as it's added later as usual as the
                    //          line ends
                    *word = '\0';
                    PRINT( "%s/%s", word_buf, line );
                    line = (line + strlen( line ));
                    word_buf[0] = '\0';
                    break;
                }
                //
                switch( c ) {
                    case '{':
                        // scope opened.
                        if( use_auto_scope ) {
                            // NOTE: this even applies in block comments!
                            // TODO: support block comments
                            scope_depth += 1;
                        }
                        break;
                        //
                    case '}':
                        // scope closed
                        // NOTE: this even applies in block comments
                        if( use_auto_scope ) {
                            if( scope_depth > 0 ) {
                                // purge any definitions from the current scope
                                tt_purge_definitions( scope_depth );
                                scope_depth -= 1;
                            } else {
                                // more }'s than {'s.  Can't be sure this is an
                                // error so we just warn and continue, the
                                // host language can deal with it
//                                fprintf(
//                                        stderr,
//                                        "[tt] %s:%zu"
//                                        "WARN: Scope depth error?\n",
//                                        in_file,
//                                        line_number
//                                    );
                                }
                        }
                        break;
                        //
                    case '"':
                    case '\'':
                        // start of quotes
                        in_q = 1;
                        q = c;
                        break;
                        //
                    case ',':
                    case ';':
                    case ' ':
                    case '(':
                        // term ended
                        *word = '\0';
                        //
                        // is the next char "(" or " (", if so - this may
                        // be a function and may need expanding
                        if( (c == '(') || ((c == ' ') && (*line == '(')) ) {
                            // it is, try resolving
                            //
                            if( (c == ' ') ) {
                                // since we were "xx (" we will skip over
                                // the " " before the "(".  It'll be
                                // added back by <tt_resolve_call()> if
                                // needed via HAS_SPACE arg
                                line += 1;
                            }
                            //
                            *warn_str = '\0';
                            if( !tt_resolve_call(
                                    word_buf, tt_has_args( line ),
                                    (c == ' '),
                                    in_file, line_number
                                ) ) {
                                SHOW_ERROR( error_str );
                                exit_result = 1;
                                goto finish;
                            }
                        } else {
                            // isn't a function call, just output as-is
                            PRINT( "%s%c", word_buf, c );
                        }
                        //
                        word = word_buf;
                        c = 0;
                        break;
                        //
//                    case '(':
//                        // function call to deal with
//                        *word = '\0';
//                        *warn_str = '\0';
//                        if( !tt_resolve_call(
//                                word_buf, tt_has_args( line ),
//                                in_file, line_number
//                            ) ) {
//                            SHOW_ERROR( error_str );
//                            exit_result = 1;
//                            goto finish;
//                        }
//                        //
//                        word = word_buf;
//                        c = 0;
//                        break;
//                        //
                }
                if( c ) {
                    // word is the same size as the line buffer so this
                    // can't overflow (we never write more than the line)
                    *(word++) = c;
                }
            }
        }
        //
        *cmp = '\0';
        *word = '\0';
        PRINT( "%s\n", word_buf );
    }
    //
finish:
    tt_pair_free( aliases );
    mxl_free( mxl );
    //
    if( output && (output != stdout) ) {
        fclose( output );
    }
    if( f && (f != stdin) ) {
        fclose( f );
    }
    return exit_result;
}
